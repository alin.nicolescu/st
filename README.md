# st patched

`st` - simple terminal implementation for X from
[suckless.org](http://st.suckless.org/).

`st-0.8.4` as a base.

```bash
sudo make install clean
```

## Patches

- [`hidecursor`](http://st.suckless.org/patches/hidecursor/)
- [`boxdraw`](http://st.suckless.org/patches/boxdraw/) for gapless alignement
- [`scrollback`](http://st.suckless.org/patches/scrollback/) with
  `Shift+PageUp`, `Shift+PageDown` and `Shift+MouseWheel`
    
    ## Configs

    - `monospace` system font with pixelsize of 14
    - `borderpx=4`
    - `cwscale=1.1`
    - `tabspaces=4`
